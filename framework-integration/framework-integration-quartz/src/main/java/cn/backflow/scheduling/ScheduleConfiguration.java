package cn.backflow.scheduling;

import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class ScheduleConfiguration {

    @Bean
    SchedulerFactoryBeanCustomizer schedulerFactoryBeanCustomizer() {
        return schedulerFactoryBean -> {
            schedulerFactoryBean.setWaitForJobsToCompleteOnShutdown(true);
            schedulerFactoryBean.setOverwriteExistingJobs(true);
            schedulerFactoryBean.setAutoStartup(true);
            schedulerFactoryBean.setStartupDelay(5);
        };
    }

    /*
    @Bean
    public JobDetail printCurrentTimeJob() {
        return newJob(PrintCurrentTimeJob.class)
                .withIdentity("printCurrentTimeJob")
                .withDescription("打印当前时间")
                .usingJobData("name", PrintCurrentTimeJob.class.getSimpleName())
                .requestRecovery(false)
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger printCurrentTimeJobTrigger() {
        return newTrigger()
                .forJob("printCurrentTimeJob")
                .withIdentity("printCurrentTimeJobTrigger")
                .withSchedule(cronSchedule("0/5 * * * * ?"))
                .build();
    }
    */

    class TestScheduleCustomizer {

    }
}
