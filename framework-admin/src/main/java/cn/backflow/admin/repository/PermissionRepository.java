package cn.backflow.admin.repository;

import cn.backflow.admin.entity.Permission;
import cn.backflow.data.repository.BaseMyBatisRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PermissionRepository extends BaseMyBatisRepository<Permission, Integer> {

    public List<String> findCode(Object parameter) {
        return sqlSession.selectList("Permission.findCode", parameter);
    }

    public List<Permission> findByIds(Collection<Integer> ids) {
        return sqlSession.selectList("Permission.findAll", Collections.singletonMap("ids", ids));
    }


    public List<Permission> findSiblings(Integer id, boolean excludeCurrent) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("excludeCurrent", excludeCurrent);
        parameter.put("id", id);
        return sqlSession.selectList("Permission.findSiblings", parameter);
    }

    /**
     * 更新所有子权限的祖先ID路径
     *
     * @param parent 父权限
     * @return 更新记录数
     */
    public int updateChildrenAncestors(Permission parent) {
        return sqlSession.update("Permission.updateChildrenAncestors", parent);
    }
}