package cn.backflow.admin.controller;

import cn.backflow.admin.service.CacheService;
import cn.backflow.admin.Configuration;
import cn.backflow.admin.Constants;
import cn.backflow.admin.entity.Dict;
import cn.backflow.admin.entity.ResetRecord;
import cn.backflow.admin.entity.User;
import cn.backflow.admin.service.DictService;
import cn.backflow.admin.service.UserService;
import cn.backflow.utils.JsonMap;
import cn.backflow.utils.Strings;
import cn.backflow.web.BaseSpringController;
import cn.backflow.web.Emails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@RestController
public class IndexController extends BaseSpringController {
    private final static Logger LOG = LoggerFactory.getLogger(IndexController.class);

    private final CacheManager cacheManager;
    private final UserService userService;
    private final DictService dictService;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public IndexController(CacheManager cacheManager, UserService userService, DictService dictService) {
        this.cacheManager = cacheManager;
        this.userService = userService;
        this.dictService = dictService;
    }

    public static User getCurrentUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute(Constants.SESSION_USER_KEY);
    }

    /**
     * 是否是验证码登录
     *
     * @param username 用户名
     * @param fail     计数加1
     * @param clean    计数清零
     */
    public static boolean isLoginWithValidateCode(String username, boolean fail, boolean clean) {
        Map<String, Integer> loginFailMap = CacheService.get(Constants.LOGIN_FAILURE_MAP);
        if (loginFailMap == null) {
            loginFailMap = new HashMap<>();
            CacheService.put(Constants.LOGIN_FAILURE_MAP, loginFailMap);
        }
        Integer loginFailNum = loginFailMap.get(username);
        if (loginFailNum == null) {
            loginFailNum = 0;
        }
        if (fail) {
            loginFailNum++;
            loginFailMap.put(username, loginFailNum);
        }
        if (clean) {
            loginFailMap.remove(username);
        }
        return loginFailNum >= 3;
    }

    /* 前端请求该方法验证用户是否登录 */
    @RequestMapping("authorizations")
    public Object authorizations(HttpServletRequest request, HttpServletResponse response) {
        User user = getCurrentUser(request);
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return JsonMap.fail("未登录或登录已超时，请重新登录。");
        }
        return userService.findPermissionByUser(user);
    }

    @RequestMapping(path = "login", method = RequestMethod.POST)
    public Object login(String name, String pass, HttpServletRequest request) {
        /* 校验登录验证码 *
        if (isLoginWithValidateCode(token.getName(), false, false)) {
			String code = (String) request.getSession().getAttribute(ImageServlet.VALIDATE_CODE);
			if (token.getCaptcha() == null || !token.getCaptcha().toUpperCase().equals(code)) {
				throw new AuthenticationException("验证码错误, 请重试.");
			}
		}
        */
        JsonMap json = JsonMap.fail();
        User user;

        if (Strings.isEmail(name)) {
            user = userService.getByEmail(name);
            json.msg("邮箱帐号输入错误或未注册.");
        } else if (Strings.isMobile(name)) {
            user = userService.getByPhone(name);
            json.msg("手机号码输入错误或未注册.");
        } else {
            user = userService.getByName(name);
        }
        if (user == null) {
            return json.msg("用户名输入错误或未注册！");
        }
        if (!Strings.md5(pass, user.getName()).equalsIgnoreCase(user.getPass())) {
            return json.msg("密码输入不正确!");
        }
        if (Constants.USER_STATUS_LOCKED == user.getState()) {
            return json.msg("用户[" + name + "]账户已被锁定, 禁止登录");
        }
        // 保存用户信息到session
        request.getSession().setAttribute(Constants.SESSION_USER_KEY, user);
        userService.updateVisited(user.getId());
        user.setPass(null);
        return json.success(true).put("user", user);
    }

    @RequestMapping("logout")
    public Object logout(HttpServletRequest request) {
        User user = getCurrentUser(request);
        CacheService.cleanUserCaches(user);
        return JsonMap.succeed();
    }

    @RequestMapping(value = "unlock", method = RequestMethod.POST)
    public Object unlock(String password, HttpServletRequest request) {
        JsonMap json = JsonMap.fail();
        User user = getCurrentUser(request);
        if (user == null)
            json.put("expired", true);
        else if (!Strings.md5(password, user.getName()).equals(user.getPass()))
            json.put("msg", "密码不正确 !");
        else {
            request.getSession().removeAttribute(Constants.USER_SCREEN_LOCKED);
            json.put("success", true);
        }
        return json;
    }


    @RequestMapping("exists")
    public Object exists(@RequestParam(value = "name", required = false) String name,
                         @RequestParam(value = "phone", required = false) String phone,
                         @RequestParam(value = "email", required = false) String email,
                         HttpServletResponse response) {
        User user = null;
        String msg = "用户名已被注册!";
        if (Strings.isNotBlank(name)) {
            user = userService.getByName(name);
        } else if (Strings.isNotBlank(email)) {
            user = userService.getByEmail(email);
            msg = "邮箱已被注册";
        } else if (Strings.isNotBlank(phone)) {
            user = userService.getByPhone(phone);
            msg = "手机号已被注册";
        }
        boolean valid = user == null;
        JsonMap json = new JsonMap();
        json.put("valid", valid);
        if (!valid) {
            json.msg(msg);
        }
        // Set response result for VeeValidate
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate"); // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT"); // Set to expire far in the past.
        response.setHeader("Pragma", "no-cache"); // Set standard HTTP/1.0 no-cache header.
        return json;
    }


    /* 重置密码 */
    @RequestMapping(path = "forgot", method = RequestMethod.POST)
    public Object forgot(HttpSession session,
                         @RequestParam(value = "email") String email,
                         @RequestParam(value = "kaptcha", required = false) String kaptcha) {
        Map<String, Object> map = new HashMap<>();
        Map<String, String> err = new HashMap<>();
        Object sessionKaptcha = session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (Strings.isBlank(email)) {
            err.put("email", "请输入邮箱帐号.");
        } else if (sessionKaptcha != null && !sessionKaptcha.equals(kaptcha)) {
            err.put("kaptcha", "验证码错误.");
        } else {
            User user = userService.getByEmail(email);
            if (user == null)
                err.put("email", "您输入的邮箱不存在.");
            else {
                try {
                    // 发送邮件
                    saveRecordAndSendEmail(user);
                    map.put("loginUrl", getEmailLoginUrl(email));
                    map.put("success", true);
                } catch (Exception e) {
                    e.printStackTrace();
                    map.put("msg", e.getMessage() + "邮件发送出错, 请稍后重试.");
                }
            }
            session.removeAttribute(Constants.KAPTCHA_SESSION_KEY);
        }
        if (err.size() > 0)
            map.put("errors", err);
        return map;
    }

    /**
     * 根据邮箱域名获得登录地址
     *
     * @param email 邮箱域名
     * @return 登录地址，找不到返回null
     */
    private String getEmailLoginUrl(String email) {
        String arr[] = email.split("@");
        if (arr.length != 2) return null;
        Dict dict = dictService.findMapByCode("email_login_url").get(arr[1]);
        return dict == null ? null : dict.getValue();
    }

    /**
     * 保存申请记录并发送密码重置链接邮件
     *
     * @param user 申请用户
     */
    private void saveRecordAndSendEmail(User user) throws Exception {
        ResetRecord resetRecord = userService.saveResetResord(user);
        String email = resetRecord.getEmail();
        String resetUrl = String.format("%s?email=%s&key=%s", Configuration.PASSWORD_RESET_URL, email, resetRecord.getResetKey());
        Emails.sendEmail(
                String.format("来自%s的密码重置邮件", Configuration.APP_NAME),
                MessageFormat.format(Configuration.PASSWORD_RESET_EMAIL_TEMPLATE, email, resetUrl),
                email
        );
    }

    @GetMapping("reset")
    public Object passwordReset(@RequestParam("email") String email,
                                @RequestParam("key") String key) {
        JsonMap json = JsonMap.succeed();
        boolean valid = userService.isResetRecordValid(email, key);
        if (!valid) {
            json.success(false).msg("链接已过期!");
        } else
            json.put("email", email);
        return valid ? "reset" : "redirect:/guest/forgot";
    }


    @PostMapping("reset")
    public Object passwordReset(@RequestParam("email") String email,
                                @RequestParam("key") String key,
                                @RequestParam("password") String password,
                                @RequestParam("password_again") String passwordAgain) {
        Map<String, Object> json = new HashMap<>();
        if (Strings.isBlank(password))
            json.put("msg", "密码不能为空或全为空格.");
        else if (!password.equals(passwordAgain))
            json.put("msg", "两次输入的密码不一致.");
        else if (password.length() < 5 || password.length() > 15)
            json.put("msg", "密码长度应为5至15位.");
        else {
            boolean valid = userService.isResetRecordValid(email, key);
            if (valid) {
                userService.resetPass(email, password, key);
                json.put("success", true);
            } else
                json.put("msg", "重置密码参数错误或已过期, 请重新申请重置.");
        }
        return json;
    }

    @DeleteMapping("reload-config")
    public Object reloadConfig() {
        try {
            Configuration.reload();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return JsonMap.fail(e.getMessage());
        }
        return JsonMap.succeed().msg("系统配置重载成功!");
    }


    @DeleteMapping("clean-cache")
    public Object cleanCache(@RequestParam(value = "name", required = false, defaultValue = Constants.PERMISSION_CACHE) String name) {
        if (Strings.isBlank(name)) {
            cacheManager.getCacheNames().forEach(cname -> {
                cacheManager.getCache(cname).clear();
                LOG.info("Cache with name [%s] cleaned !", cname);
            });
        } else {
            Cache cache = cacheManager.getCache(name);
            if (cache != null) {
                cache.clear();
                LOG.info("Cache with name [%s] cleaned !", name);
            }
        }
        return JsonMap.succeed().msg("系统缓存清理成功!");
    }
}
