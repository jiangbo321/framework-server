package cn.backflow.data.repository;

import cn.backflow.data.entity.BaseEntity;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class BaseMyBatisRepository<E extends BaseEntity, PK extends Serializable> implements BaseRepository<E, PK> {

    private Class<E> entityClass = getEntityClass();

    protected SqlSessionTemplate sqlSession;

    @Autowired
    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }

    @SuppressWarnings("unchecked")
    private Class<E> getEntityClass() {
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<E>) (parameterizedType.getActualTypeArguments()[0]);
    }

    @Override
    public Object select(String sql) {
        return sqlSession.update(entityClass.getSimpleName() + ".select", sql);
    }

    @Override
    public void flush() { /* ignore */ }

    /**
     * 用于子类覆盖,在insert,update之前调用
     */
    private void prepareForSaveOrUpdate(E o) {
    }

    private String getFindAllQuery() {
        return entityClass.getSimpleName() + ".findAll";
    }

    private String getPageQuery() {
        return entityClass.getSimpleName() + ".paging";
    }

    private String getFindByPrimaryKeyQuery() {
        return entityClass.getSimpleName() + ".getById";
    }

    private String getInsertQuery() {
        return entityClass.getSimpleName() + ".insert";
    }

    private String getInsertBatchQuery() {
        return entityClass.getSimpleName() + ".insertBatch";
    }

    private String getUpdateQuery() {
        return entityClass.getSimpleName() + ".update";
    }

    private String getUpdateSelectiveQuery() {
        return entityClass.getSimpleName() + ".updateSelective";
    }

    private String getDeleteQuery() {
        return entityClass.getSimpleName() + ".delete";
    }

    private String getBatchDeleteQuery() {
        return entityClass.getSimpleName() + ".deleteBatch";
    }

    private String getCountQuery(String statement) {
        return statement == null ? getPageQuery() : statement + "Count";
    }

    @Override
    public E getById(PK primaryKey) {
        return sqlSession.selectOne(getFindByPrimaryKeyQuery(), primaryKey);
    }

    @Override
    public int deleteById(PK id) {
        return sqlSession.delete(getDeleteQuery(), id);
    }

    @Override
    public int deleteBatch(Collection<PK> pks) {
        return sqlSession.delete(getBatchDeleteQuery(), pks);
    }

    @Override
    public int insert(E entity) {
        prepareForSaveOrUpdate(entity);
        return sqlSession.insert(getInsertQuery(), entity);
    }

    public int insertBatch(List<E> entities) {
        return sqlSession.insert(getInsertBatchQuery(), entities);
    }

    @Override
    public int update(E entity) {
        prepareForSaveOrUpdate(entity);
        return sqlSession.update(getUpdateQuery(), entity);
    }

    @Override
    public int updateBatch(List<E> entities) {

        int i = 0;
        try (SqlSession sqlSession = this.sqlSession.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            for (E entity : entities) {
                i += sqlSession.update(getUpdateQuery(), entity);
            }
            sqlSession.commit();
            return i;
        }
    }

    @Override
    public int updateSelectiveBatch(List<E> entities) {
        int i = 0;
        try (SqlSession sqlSession = this.sqlSession.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            for (E entity : entities) {
                i += sqlSession.update(getUpdateSelectiveQuery(), entity);
            }
            sqlSession.commit();
            return i;
        }
    }

    @Override
    public int updateSelective(E entity) {
        return sqlSession.update(getUpdateSelectiveQuery(), entity);
    }

    public int saveOrUpdate(E entity) {
        prepareForSaveOrUpdate(entity);
        return entity.getId() == null ? insert(entity) : update(entity);
    }

    @Override
    public boolean isUnique(String uniquePropertyNames) {
        throw new UnsupportedOperationException();
    }

    public List<E> findAll(Object parameter) {
        return sqlSession.selectList(getFindAllQuery(), parameter);
    }

    public <T> Map<T, E> findMap(Object parameter, String mapKey) {
        return sqlSession.selectMap(getFindAllQuery(), parameter, mapKey);
    }

    public Page<E> findByPageRequest(PageRequest pageRequest) {
        return pageQuery(getPageQuery(), pageRequest);
    }

    protected Page<E> pageQuery(String statement, PageRequest pr) {
        Long count = sqlSession.selectOne(getCountQuery(statement), pr.getFilters());
        Page<E> page = new Page<>(pr, count.intValue());
        if (count > 0) {
            pr.getFilters().put("_offset_", page.getFirstElementIndex());
            pr.getFilters().put("_limit_", page.getPageSize());
            pr.getFilters().put("_sort_", pr.getSortColumns());
            List<E> list = sqlSession.selectList(statement, pr.getFilters());
            // List<E> list = sqlSession.selectList(
            // statementName, pr.getFilters(),
            // new RowBounds(page.getFirstElementIndex(), page.getPageSize())
            // );
            page.setItems(list);
        }
        return page;
    }

}